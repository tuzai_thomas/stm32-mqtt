#ifndef __USRCONFIG_H__
#define __USRCONFIG_H__


#define TIMER3_PRESCALER    7199
#define TIMER3_PERIOD       9999
#define TIMER4_PRESCALER    7199
#define TIMER4_PERIOD       19999

#define USR_RX_DMA_LEN_MAX  2048



#define USR_WIFI_SSID   
#define USR_WIFI_PWD    

#define USR_IP          
#define USR_PORT        1883
#define USR_CLIENT_ID   
#define USR_NAME        
#define USR_PWD         

#define USR_MQTT_SUB_TOPIC  "/sys/a1hPCyYvI13/dREDqtrVpJcae0pXGjkI/thing/event/property/post"
#define USR_MQTT_PUB_TOPIC  "/sys/a1hPCyYvI13/dREDqtrVpJcae0pXGjkI/thing/event/property/post"


enum{
    USR_MQTT_STATE_CONNECTED = 1,
    USR_MQTT_STATE_SUBACK,
};


#endif

