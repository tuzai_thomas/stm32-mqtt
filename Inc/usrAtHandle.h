#ifndef __USRATHANDLE_H__
#define __USRATHANDLE_H__


#define SSID_MAX_LEN    20
#define PWD_MAX_LEN     20
#define IP_MAX_LEN      15


enum{
    AT_CMD_TEST = 0,
    AT_CMD_GMR,
    AT_CMD_CWMODE,
    AT_CMD_CWJAP,
    AT_CMD_CIFSR,
    AT_CMD_CIPSTART,
    AT_CMD_CIPMODE,
    AT_CMD_CIPSEND,

    AT_CIP_SEND_DATA,
    AT_CIP_END,

    AT_CMD_MAX
};

typedef struct wifiConfig{
    char ssid[SSID_MAX_LEN];
    char pwd[PWD_MAX_LEN];
}wifiConfig_t;

typedef struct tcpConfig
{
    uint16_t port;
    char ip[IP_MAX_LEN];
}tcpConfig_t;


int usrSend(int fd, char* p_dat, size_t len, int flags);
int usrRecv(int fd, char* buf, size_t len, int flags);
uint8_t usrSendAtCmd(uint8_t cmdId, char* p_dat, int len);
void usrAtInit(void* p1, void* p2);

#endif
