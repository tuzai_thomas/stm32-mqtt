#ifndef __USRlIST_H__
#define __USRlIST_H__


#define MAX_SIZE 6
#define MAX_ELEMENT_SIZE 200

// 队列元素结构体
typedef struct {
    char data[MAX_ELEMENT_SIZE];
    int length;
} QueueElement;

// 队列结构体
typedef struct {
    QueueElement elements[MAX_SIZE];
    int front;
    int rear;
} Queue;


uint8_t enqueue(Queue *q, char *item, int length);
QueueElement* dequeue(Queue *q);
void usrQueueInit(Queue* q);

#endif
